DROP DATABASE IF EXISTS SmartInfo
;
CREATE DATABASE SmartInfo;
USE SmartInfo;

CREATE TABLE Users (
	userID int (10) primary key not null auto_increment,
	username varchar (10) NOT NULL,
    password varchar (50) NOT NULL,
    salt varchar (30) NOT NULL,
    user_type int (1) NOT NULL
);
CREATE TABLE Courses (
	courseID integer NOT NULL primary key auto_increment,
	course_name varchar(7) NOT NULL
) 
;

CREATE TABLE Relationships (
	rel_id int(10) NOT NULL primary key auto_increment,
    person int(10) NOT NULL,
    course int(10) NOT NULL,
	FOREIGN KEY fk1(person) REFERENCES Users(userID),
    FOREIGN KEY fk2(course) REFERENCES Courses(courseID)
);

CREATE TABLE Grades(
	assignmentID int (10) primary key not null auto_increment,
    score int (3),
    letter varchar (2) not null,
    assignmentName varchar (50) not null,
    relation int(10) not null,
	FOREIGN KEY fk1(relation) REFERENCES Relationships(rel_id)
)
;

CREATE TABLE Announcements(
	a_id int(10) NOT NULL primary key auto_increment,
    #visibility 0 = course only 1 = EVERYONE
    visibility int (1) NOT NULL,
    title varchar(50) NOT NULL,
    info varchar(500) NOT NULL,
    person int(10) NOT NULL,
    course int(10),
    FOREIGN KEY fk(person) REFERENCES Users(userID),
    FOREIGN KEY fk3(course) REFERENCES Courses(courseID)
    
    
);

