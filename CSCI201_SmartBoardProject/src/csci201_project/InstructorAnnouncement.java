package csci201_project;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class InstructorAnnouncement extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fonts and Color
	private int fontSizeSmall = 24;
	private int fontSizeLarge = 48;
	private String fontName = "SANS_SERIF";
	private Color dark, light, pink;

	// JObjects
	private JPanel pageTitlePanel, enterTitlePanel, messagePanel, mainPanel;
	private JLabel pageTitleLabel, enterTitleLabel, messageLabel;
	private JTextArea announcementTextArea;
	private JTextField titleTextField;
	private JButton createButton;

	// Course
	private Course c;
	private Socket s;
	private ObjectOutputStream oos;

	public InstructorAnnouncement(Course c, Socket s, ObjectOutputStream oos) {
		this.c = c;
		this.s = s;
		this.oos = oos;
		initializeVariables();
		createGUI();
	}

	public void initializeVariables() {
		dark = new Color(255, 250, 240);
		light = new Color(0, 0, 0);
		pink = new Color(135, 206, 250);

		// JLabels
		pageTitleLabel = new JLabel("Make Announcement");
		enterTitleLabel = new JLabel("Title: ");
		messageLabel = new JLabel("Message: ");

		pageTitleLabel.setForeground(light);
		enterTitleLabel.setForeground(light);
		messageLabel.setForeground(light);

		pageTitleLabel.setFont(new Font(fontName, Font.BOLD, fontSizeLarge));
		enterTitleLabel.setFont(new Font(fontName, Font.BOLD, fontSizeSmall));
		messageLabel.setFont(new Font(fontName, Font.BOLD, fontSizeSmall));

		// JPanels
		pageTitlePanel = new JPanel();
		enterTitlePanel = new JPanel();
		messagePanel = new JPanel();
		mainPanel = new JPanel();

		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));

		pageTitlePanel.setBackground(dark);
		enterTitlePanel.setBackground(dark);
		messagePanel.setBackground(dark);
		mainPanel.setBackground(dark);

		// Text Objects
		titleTextField = new JTextField(20);
		announcementTextArea = new JTextArea(20, 40);
		announcementTextArea.setLineWrap(true);
		announcementTextArea.setWrapStyleWord(true);
		announcementTextArea.setBorder(BorderFactory
				.createLineBorder(Color.BLACK));

		// JButtons
		createButton = new JButton("Create");
		createButton.setBackground(pink);
		createButton.addActionListener(this);
	}

	public void createGUI() {
		pageTitlePanel.add(pageTitleLabel);

		enterTitlePanel.add(enterTitleLabel);
		enterTitlePanel.add(titleTextField);

		messagePanel.add(messageLabel);
		messagePanel.add(announcementTextArea);

		this.setLayout(new BorderLayout());
		this.add(pageTitlePanel, BorderLayout.NORTH);

		mainPanel.add(enterTitlePanel);
		mainPanel.add(messagePanel);

		this.add(mainPanel, BorderLayout.CENTER);

		createButton.setPreferredSize(new Dimension(70, 70));
		createButton.setFont(new Font(fontName, Font.BOLD, fontSizeSmall));
		this.add(createButton, BorderLayout.SOUTH);
		// this.add(messagePanel, BorderLayout.SOUTH);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == createButton) {
			if (announcementTextArea.getText().trim().equals("")
					|| titleTextField.getText().trim().equals("")) {
				JOptionPane.showMessageDialog(this,
						"Please fill out all fields!", "Announcement Error",
						JOptionPane.ERROR_MESSAGE);
			} else {
				Announcement ann = new Announcement(titleTextField.getText(),
						null, announcementTextArea.getText(),
						c.get_course_name(), "Course Announcement");
				Request r = new Request("announcement", ann);
				try {
					oos.writeObject(r);
					oos.flush();
				} catch (IOException ioe) {
					System.out.println("IOE in InsructorAnnouncement click: "
							+ ioe.getMessage());
				}
				
				titleTextField.setText("");
				announcementTextArea.setText("");
			}
		}
	}

}
