package csci201_project;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

/*
 * Also has a space that's looking for an announcement
 */

public class StudentHomePage extends JFrame implements ActionListener {

	JPanel jp1, jp2, jp3, jp4, jp5, newsFeed;
	JLabel newsFeedLabel, username, sample1, sample2, sample3, sample4,
			sample5;
	JButton logout, selectClass;
	List<JLabel> list = new ArrayList<JLabel>();
	// JTable tables;

	JComboBox<String> userBox;
	Vector<String> userClassesTitles = new Vector<String>();

	JScrollPane jsp;

	private String fontName = "SANS_SERIF";
	private int fontSize = 48;
	private int fontSize2 = 24;

	Color labels;
	Color background;
	Color buttons;

	String selectedClass = "";

	private Student currentUser;
	private Socket s;
	private ObjectOutputStream oos;
	private ObjectInputStream ois;

	public StudentHomePage(User stu, Socket s, ObjectOutputStream oos,
			ObjectInputStream ois, ArrayList<Announcement> pubAnnouncements) {
		super("SmartBoard");
		currentUser = (Student) stu;
		setSize(1000, 700);
		setLocation(150, 60);
		setResizable(false);
		setClose();

		this.s = s;
		this.oos = oos;
		this.ois = ois;

		background = new Color(255, 250, 240);
		labels = new Color(0, 0, 0);
		buttons = new Color(135, 206, 250);

		jp1 = new JPanel();
		jp2 = new JPanel();
		jp3 = new JPanel();
		jp4 = new JPanel();
		jp5 = new JPanel();
		newsFeed = new JPanel();

		jp1.setLayout(new BorderLayout(50, 50));
		jp1.setBackground(background);
		jp2.setBackground(background);
		jp3.setBackground(background);
		jp4.setBackground(background);
		jp5.setBackground(background);

		newsFeedLabel = new JLabel("News Feed");
		newsFeedLabel.setForeground(labels);
		newsFeedLabel.setFont(new Font(fontName, Font.BOLD, fontSize));
		newsFeedLabel.setBackground(labels);
		newsFeedLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		if (currentUser != null)
			username = new JLabel(currentUser.get_username());
		else
			username = new JLabel("Username");
		username.setForeground(labels);
		username.setBackground(labels);
		username.setFont(new Font(fontName, Font.BOLD, fontSize2));

		// jsp = new JScrollPane(newsFeed,
		// JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
		// JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		jsp = new JScrollPane(newsFeed, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		jsp.getViewport().add(newsFeed);
		jsp.setSize(200, jsp.getHeight());
		newsFeed.setSize(200, jsp.getHeight());

		jsp.validate();

		logout = new JButton("Logout");
		logout.addActionListener(this);
		selectClass = new JButton("Go");
		selectClass.addActionListener(this);

		// list.add(sample1);
		// list.add(sample2);
		// list.add(sample3);
		// list.add(sample4);
		// list.add(sample5);
		// newsFeed.add(sample1);
		// newsFeed.setBorder(BorderFactory.createLineBorder(Color.BLACK, 5));
		// newsFeed.add(sample2);
		// newsFeed.add(sample3);
		// newsFeed.add(sample4);
		// newsFeed.add(sample5);
		// newsFeed.setLayout(new BoxLayout((Container)newsFeed,
		// BoxLayout.Y_AXIS));
		// jsp.setViewportView(newsFeed);
		jsp.setBackground(buttons);
		newsFeed.setBackground(buttons);
		// newsFeed.add(new JLabel ("THINGYS"));
		// newsFeed.add(new JLabel ("FUCK YOU"));
		newsFeed.setLayout(new GridBagLayout());

		jp2.setLayout(new BorderLayout());
		jp4.add(newsFeedLabel);
		jp2.add(jp4, BorderLayout.NORTH);
		jp2.add(jsp, BorderLayout.CENTER);
		jp1.add(jp2, BorderLayout.CENTER);

		userBox = new JComboBox<String>(userClassesTitles);

		if (currentUser.get_all_courses() != null
				&& currentUser.get_all_courses().size() > 0) {
			for (String c : currentUser.get_all_courses().keySet()) {
				userClassesTitles.add(c);
			}
			userBox.setSelectedIndex(0);
		}
		jp3.setLayout(new BorderLayout());
		jp5.add(username);
		jp5.add(userBox);
		jp5.add(selectClass);
		jp3.add(jp5, BorderLayout.NORTH);
		jp3.add(logout, BorderLayout.SOUTH);
		jp1.add(jp3, BorderLayout.EAST);

		add(jp1);
		
		for(int i = 0; i < pubAnnouncements.size(); i++){
			this.giveAnnouncement(pubAnnouncements.get(i));
			System.out.println(pubAnnouncements.get(i).getAnnouncementTitle());
		}
	}

	private void setClose() {
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				try {
					Request r = new Request("close", "close");
					oos.writeObject(r); // tell server to close client's
					ois.close();
					oos.close();
					System.out.println("Closed streams");
				} catch (IOException e1) {
					System.out
							.println("IOE in LoginPage addListeners() window operation: "
									+ e1.getMessage());
				} finally {
					System.exit(0);
				}
			}
		});
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == selectClass) {
			selectedClass = (String) userBox.getSelectedItem();
			System.out.println(selectedClass);
			Course c = currentUser.get_single_course(selectedClass);
			CoursePage cp = new CoursePage(currentUser, s, oos, ois, c);
			this.setVisible(false);
			cp.setVisible(true);

		}
		if (e.getSource() == logout) {
			LoginPage lp = new LoginPage();
			this.setVisible(false);
			lp.setVisible(true);
		}
	}

	public void giveAnnouncement(Announcement ann) {// Gets announcement sent to
													// all clients
		String courseName = ann.getAnnouncementCourse();
		Course c = currentUser.get_single_course(courseName);

		if (c != null || ann.getAnnouncementCourse().equals("university")) {
			ContentPaneManager.addContent(newsFeed, ann);
			// newsFeed.add(new JLabel("ANNOUNCEMENT SHOULD BE HERE! "));
			newsFeed.validate();
			newsFeed.repaint();
			System.out.println(ann.getAnnouncementComment());
			jsp.getViewport().add(newsFeed);
			jsp.validate();
			jp2.validate();
			jp2.repaint();
			jp1.validate();
			jp1.repaint();
		}
	}

}
