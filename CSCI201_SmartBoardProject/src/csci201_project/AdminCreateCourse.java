package csci201_project;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/*
 * Hi Robyn. I added to the action listener a constructor for an object. I also left the code that was there before. 
 * So like take a look at line 186. It has some things that lazy me did at 2:20 am.
 * 
 * Justine
 * 
 */

public class AdminCreateCourse extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	static AdminCreateCourse aa;

	JPanel jp1, jp2, jp3, jp4, jp5, jp6, jp7, jp8;
	JLabel courseLabel, courseTitle;
	JTextField announcementContentTitleTF;
//	JTextField announcementText;
	JButton done;
	JButton back;
	Color darker, lighter, pink;
	JComboBox<String> userBox;
	JRadioButton student, instructor;
	Vector<String> userType = new Vector<String>();
	private String fontName = "SANS_SERIF";
	private int fontSize = 48;
	private int fontSize2 = 24;

	private Socket s;
	private ObjectOutputStream oos;
	private ObjectInputStream ois;
	
	private AdminHomePage parent;

	public AdminCreateCourse(AdminHomePage parent, Socket s, ObjectOutputStream oos, ObjectInputStream ois) {
		super("SmartBoard");
		this.s = s; // Added by Robyn
		this.oos = oos;
		this.ois = ois;
		setSize(1000, 700);
		setLocation(150, 60);
		setResizable(false);
		setCloseOp(); // Added by Robyn
		darker = new Color(255, 250, 240);
		lighter = new Color(0, 0, 0);
		pink = new Color(135, 206, 250);

		this.parent = parent;
		
		jp1 = new JPanel();
		jp2 = new JPanel();
		jp3 = new JPanel();
		jp4 = new JPanel();
		jp5 = new JPanel();
		jp6 = new JPanel();
		jp7 = new JPanel();
		jp8 = new JPanel();

		jp1.setLayout(new BorderLayout(50, 50));
		jp1.setBackground(darker);
		jp2.setBackground(darker);
		jp3.setBackground(darker);
		jp4.setBackground(darker);
		jp5.setBackground(darker);
		jp6.setBackground(darker);
		jp7.setBackground(darker);
		jp8.setBackground(darker);

	
		
		courseTitle = new JLabel("Course Name: ");
		courseTitle.setFont(new Font(fontName, Font.BOLD, fontSize2));
		courseTitle.setForeground(lighter);

		announcementContentTitleTF = new JTextField(20);
		
	
		done = new JButton("Create");
		done.setBackground(pink);
		done.setOpaque(true);
		done.setMinimumSize(new Dimension(50, 50));
		done.setMaximumSize(new Dimension(50, 50));
		done.setPreferredSize(new Dimension(50, 50));
		done.addActionListener(this);

		back = new JButton("<-");
		jp8.setAlignmentX(Component.LEFT_ALIGNMENT);
		jp8.add(back);
		back.addActionListener(this);

		jp2.setLayout(new FlowLayout(1, 20, 20));
		jp2.add(courseTitle);
		jp2.add(announcementContentTitleTF);
		jp3.setLayout(new FlowLayout(1, 40, 40));
		
		JLabel pageTitleLabel = new JLabel("Create Course");
		pageTitleLabel.setFont(new Font(fontName, Font.BOLD, fontSize));;
		pageTitleLabel.setForeground(lighter);
		
		JPanel pageTitlePanel = new JPanel();
		pageTitlePanel.setBackground(darker);
		pageTitlePanel.add(pageTitleLabel);
		
		jp7.setLayout(new GridLayout(5, 1));
		jp7.setSize(200, 200);
		jp7.add(pageTitlePanel);
		jp7.add(jp2);
		jp7.add(jp3);

		jp1.add(jp8, BorderLayout.NORTH);
		jp1.add(jp7, BorderLayout.CENTER);
		jp1.add(done, BorderLayout.SOUTH);
		add(jp1);
	}

	private void setCloseOp() {
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				try {
					Request r = new Request("close", "close");
					oos.writeObject(r); // tell server to close client's
					ois.close();
					oos.close();
					System.out.println("Closed streams");
				} catch (IOException e1) {
					System.out
							.println("IOE in LoginPage addListeners() window operation: "
									+ e1.getMessage());
				} finally {
					System.exit(0);
				}
			}
		});
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Request r;
		if (e.getSource() == done) {
			r = new Request("create course", announcementContentTitleTF.getText());
			announcementContentTitleTF.setText("");
			try {
				oos.writeObject(r);
				oos.flush();
			} catch (IOException e1) {
				System.out.println("IOE in AdminCreateCourse.createClicked: " + e1.getMessage());
			}
			
		} else if (e.getSource() == back) {
			this.setVisible(false);
			parent.setVisible(true);
		}
	}


	public void errorCourseExists() {
		JOptionPane.showMessageDialog(this,
				"Course already exists",
				"Course already exists!",
				JOptionPane.ERROR_MESSAGE);
	}

}