package csci201_project;

import javax.swing.*;
import java.awt.*;


public class ContentPaneManager {
	
	private static int announcementPosition = 0;
	private static int assignmentPosition = 0;
	private static int gradePosition = 0;
	private static int lectureContentPosition = 0;
	
	public ContentPaneManager() {}
	
	public static void addContent(JPanel contentPanel, ContentObject co) {
		 GridBagConstraints gbc = new GridBagConstraints();
		 gbc.gridx = 0;
		 
		 ContentBox newContentBox = null;
     //   JOptionPane.showMessageDialog(null, "I JUST USED CONTENT OBJECT!");

        if(co instanceof Announcement) {
			newContentBox = new AnnouncementBox( (Announcement) co );
			gbc.gridy = announcementPosition++;
		} else if(co instanceof Assignment) {
			newContentBox = new AssignmentBox( (Assignment) co );
			gbc.gridy = assignmentPosition++;
		} else if(co instanceof Grade) {
			newContentBox = new GradeBox( (Grade) co );
			gbc.gridy = gradePosition++;
		} else if(co instanceof LectureContent) {
			newContentBox = new LectureContentBox( (LectureContent) co );
			gbc.gridy = lectureContentPosition++;
		}
		
		contentPanel.add(newContentBox, gbc);
	}
}
