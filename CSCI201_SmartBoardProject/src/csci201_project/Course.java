package csci201_project;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;

public class Course implements Serializable{
	private static final long serialVersionUID = 1L;
	private String course_name;
	private int courseID;
	private Instructor inst;
	private File syllabus;
	private Hashtable<String, Student> students;
	private Hashtable<String, Grade> grades;
	private ArrayList<Announcement> announcements;

	public Course(String course_name, int courseID, Instructor inst){
		this.course_name = course_name;
		this.courseID = courseID;
		this.inst = inst;
		this.students = new Hashtable<String, Student>();
		this.grades = new Hashtable<String, Grade>();
		this.announcements = new ArrayList<Announcement>();
	}
	
	public void add_grade(String username, Grade grade) {
		grades.put(username, grade);
	}
	public void remove_student(String username){
		students.remove(username);
	}
	public void add_one_student(Student u1) {
		students.put(u1.get_username(), u1);
	}
	public void add_students(Hashtable students) {
		this.students = students;
	}
	public void add_announcement(Announcement announcement){
		announcements.add(announcement);
	}
	public String get_course_name() {
		return course_name;
	}
	public Instructor getInstructor(){
		return inst;
	}
	public void setInstructor(Instructor i){
		this.inst = i;
	}
	public Hashtable<String, Student> getStudentHashtable(){
		return students;
	}
}
