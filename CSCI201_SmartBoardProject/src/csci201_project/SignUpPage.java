package csci201_project;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class SignUpPage extends JFrame implements ActionListener {

	static SignUpPage sup;

	JPanel jp1, jp2, jp3, jp4, jp5, jp6, jp7, jp8;
	JLabel username, password, email, confirmPassword, pageTitleLabel;
	JTextField jtf1, jtf2, jtf3, jtf4;
	JButton done, back;
	Color darker;
	Color lighter;
	Color pink;
	JComboBox<String> userBox;
	JRadioButton student;
	JRadioButton instructor;
	private ButtonGroup buttonGroup;
	Vector<String> userType = new Vector<String>();
	private String fontName = "SANS_SERIF";
	private int fontSize = 48;
	private int fontSize2 = 24;

	private Socket s;
	private ObjectOutputStream oos;
	private ObjectInputStream ois;
	private User currentUser;
	private AdminHomePage parent;

	public SignUpPage(AdminHomePage parent, User u, Socket s, ObjectOutputStream oos, ObjectInputStream ois) {
		super("SmartBoard");
		this.s = s; // Added by Robyn
		this.oos = oos;
		this.ois = ois;
		this.currentUser = u;
		setSize(1000, 700);
		setLocation(150, 60);
		setResizable(false);
		setCloseOp(); // Added by Robyn
		darker = new Color(255, 250, 240);
		lighter = new Color(0, 0, 0);
		pink = new Color(135, 206, 250);
		
		this.parent = parent;

		jp1 = new JPanel();
		jp2 = new JPanel();
		jp3 = new JPanel();
		jp4 = new JPanel();
		jp5 = new JPanel();
		jp6 = new JPanel();
		jp7 = new JPanel();
		jp8 = new JPanel();

		jp1.setLayout(new BorderLayout(50, 50));
		jp1.setBackground(darker);
		jp2.setBackground(darker);
		jp3.setBackground(darker);
		jp4.setBackground(darker);
		jp5.setBackground(darker);
		jp6.setBackground(darker);
		jp7.setBackground(darker);
		jp8.setBackground(darker);

		username = new JLabel("username:");
		username.setFont(new Font(fontName, Font.BOLD, fontSize2));
		username.setForeground(lighter);
		password = new JLabel("password:");
		password.setFont(new Font(fontName, Font.BOLD, fontSize2));
		password.setForeground(lighter);
		email = new JLabel(".edu email:");
		email.setFont(new Font(fontName, Font.BOLD, fontSize2));
		email.setForeground(lighter);
		confirmPassword = new JLabel("confirm password:");
		confirmPassword.setFont(new Font(fontName, Font.BOLD, fontSize2));
		confirmPassword.setForeground(lighter);
		pageTitleLabel = new JLabel("Create User");
		pageTitleLabel.setFont(new Font(fontName, Font.BOLD, fontSize));
		pageTitleLabel.setForeground(lighter);

		student = new JRadioButton("Student");
		instructor = new JRadioButton("Instructor");
		buttonGroup = new ButtonGroup();
		buttonGroup.add(student);
		buttonGroup.add(instructor);
		
		jtf1 = new JTextField(20);
		jtf2 = new JTextField(20);
		jtf3 = new JTextField(20);
		jtf4 = new JTextField(20);
		jtf1.setMinimumSize(new Dimension(200, 40));
		jtf1.setMaximumSize(new Dimension(200, 40));
		jtf1.setPreferredSize(new Dimension(200, 40));
		jtf2.setMinimumSize(new Dimension(200, 40));
		jtf2.setMaximumSize(new Dimension(200, 40));
		jtf2.setPreferredSize(new Dimension(200, 40));
		jtf1.setMinimumSize(new Dimension(200, 40));
		jtf3.setMaximumSize(new Dimension(200, 40));
		jtf3.setPreferredSize(new Dimension(200, 40));
		jtf1.setMinimumSize(new Dimension(200, 40));
		jtf3.setMaximumSize(new Dimension(200, 40));
		jtf3.setPreferredSize(new Dimension(200, 40));
		jtf4.setMinimumSize(new Dimension(200, 40));
		jtf4.setMaximumSize(new Dimension(200, 40));
		jtf4.setPreferredSize(new Dimension(200, 40));

		done = new JButton("Create");
		done.setBackground(pink);
		done.setOpaque(true);
		done.setMinimumSize(new Dimension(50, 50));
		done.setMaximumSize(new Dimension(50, 50));
		done.setPreferredSize(new Dimension(50, 50));
		done.addActionListener(this);

		back = new JButton("<-");
		jp8.setAlignmentX(Component.LEFT_ALIGNMENT);
		jp8.add(back);
		back.addActionListener(this);

		jp2.setLayout(new FlowLayout(1, 20, 20));
		jp2.add(username);
		jp2.add(jtf1);
		jp3.setLayout(new FlowLayout(1, 20, 20));
		jp3.add(email);
		jp3.add(jtf2);
		jp4.setLayout(new FlowLayout(1, 20, 20));
		jp4.add(password);
		jp4.add(jtf3);
		jp5.setLayout(new FlowLayout(1, 20, 20));
		jp5.add(confirmPassword);
		jp5.add(jtf4);
		jp6.setLayout(new FlowLayout(1, 20, 20));
		jp6.add(student);
		jp6.add(instructor);

		JPanel titlePanel = new JPanel();
		titlePanel.add(pageTitleLabel);
		titlePanel.setBackground(darker);
		
		jp7.setLayout(new GridLayout(6, 1));
		jp7.setSize(200, 200);
		jp7.add(titlePanel);
		jp7.add(jp2);
		jp7.add(jp3);
		jp7.add(jp4);
		jp7.add(jp5);
		jp7.add(jp6);

		jp1.add(jp8, BorderLayout.NORTH);
		jp1.add(jp7, BorderLayout.CENTER);
		jp1.add(done, BorderLayout.SOUTH);
		add(jp1);
	}

	private void setCloseOp() {
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				try {
					Request r = new Request("close", "close");
					oos.writeObject(r); // tell server to close client's
					ois.close();
					oos.close();
					System.out.println("Closed streams");
				} catch (IOException e1) {
					System.out
							.println("IOE in LoginPage addListeners() window operation: "
									+ e1.getMessage());
				} finally {
					System.exit(0);
				}
			}
		});
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Student newStudent;
		Instructor newInstructor;
		Request r;
		if (e.getSource() == done) {
			String username = "";

			if (jtf1.getText().trim().equals("")
					|| jtf1.getText().contains(" ")
					|| jtf1.getText().matches(".*[0-9].*")) {
				JOptionPane.showMessageDialog(this,
						"You must enter a username with no spaces or numbers.",
						"Create user error", JOptionPane.ERROR_MESSAGE);
			} else {
				username = jtf1.getText();
				if (student.isSelected()) {
					newStudent = new Student(1, username);
					jtf1.setText("");
					r = new Request("new user", newStudent);
					try {
						oos.writeObject(r);
						oos.flush();
					} catch (IOException e1) {
						System.out.println("IOE in SignUpPage create student: "
								+ e1.getMessage());
//					
					}
				} else if (instructor.isSelected()) {
					newInstructor = new Instructor(1, username);
					jtf1.setText("");
					r = new Request("new user", newInstructor);
					try{
						oos.writeObject(r);
						oos.flush();
					} catch (IOException e1) {
						System.out.println("IOE in SUP create instructor: " + e1.getMessage());
					}
				} else {
					JOptionPane.showMessageDialog(this,
							"You must select student or instructor.",
							"Create user error", JOptionPane.ERROR_MESSAGE);
				}
			}
		} else if (e.getSource() == back) {
			this.setVisible(false);
			parent.setVisible(true);
		}

	}
	
	public void errorUserExists() {
		JOptionPane.showMessageDialog(this,
				"User already exists",
				"User already exists!",
				JOptionPane.ERROR_MESSAGE);
	}

}
