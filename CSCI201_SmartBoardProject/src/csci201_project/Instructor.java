package csci201_project;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
 
public class Instructor extends User implements Serializable{
	private static final long serialVersionUID = 1L;
	private Hashtable<String, Course> courses; // keytype = String for course name value = Course 
	private ArrayList<Announcement> announcements;
	
	public Instructor(int id, String username) {
		super(id, username);
		courses = new Hashtable<String, Course>();
		announcements = new ArrayList<Announcement>();
	}

	public void add_course(Course c){
		courses.put(c.get_course_name(), c);
	}
	
	public void add_announcement(Announcement a){
		announcements.add(a);
	}
	
	@Override
	public void openHomePage() {
		// TODO Auto-generated method stub
		
	}
	
	public void make_announcement(Announcement ann, Course course){
		course.add_announcement(ann);
	}
	
	public ArrayList<Announcement> get_announcement() {
		return announcements;
	}
	
	public Hashtable<String, Course> get_all_courses() {
		return courses;
	}

	public Course get_single_course(String selectedClass) {
		return courses.get(selectedClass);
	}
}


