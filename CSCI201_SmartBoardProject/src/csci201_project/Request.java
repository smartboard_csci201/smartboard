package csci201_project;

import java.io.Serializable;


public class Request implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String type;
	private Object obj;
	public Request(String type, Object obj){
		this.type = type;
		this.obj = obj;
	}
	
	public String getRequestType(){
		return this.type;
	}
	
	public Object getObj(){
		return this.obj;
	}
}
