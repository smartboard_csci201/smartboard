package csci201_project;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

// Down yonder (line 208)  is a comment that has a need for an announcement called ao
// Also the JScrollPane (jsp) is not attached to anything...
// Also also I assumed the place that the announcements go to is jp1
// MIGHT WANT TO DOUBLE CHECK THAT

public class InstructorHomePage extends JFrame implements ActionListener {

	JPanel jp1, jp2, jp3, jp4, jp5, jp6, newsFeed;
	JLabel courses, username;
	JButton sample1, sample2, sample3, logout, selectClass;
	List<JLabel> list = new ArrayList<JLabel>();
	JTable tables;

	JComboBox<String> userBox;
	Vector<String> userClassesTitles = new Vector<String>();
	JScrollPane jsp;

	private String fontName = "SANS_SERIF";
	private int fontSize = 48;
	private int fontSize2 = 24;

	Color labels;
	Color background;
	Color buttons;

	String selectedClass = "";

	private Instructor currentUser;
	private Socket s;
	private ObjectOutputStream oos;
	private ObjectInputStream ois;
	private CoursePage cp;
	
	ArrayList<JButton> courseButtons = new ArrayList<JButton>();
	
	public InstructorHomePage(User u, Socket s, ObjectOutputStream oos,
			ObjectInputStream ois) {
		super("SmartBoard");
		setSize(1000, 700);
		setLocation(150, 60);
		setResizable(false);
		setClose();

		this.currentUser = (Instructor) u;
		this.s = s;
		this.oos = oos;
		this.ois = ois;

		background = new Color(255, 250, 240);
		labels = new Color(0, 0, 0);
		buttons = new Color(135, 206, 250);

		jp1 = new JPanel();
		jp2 = new JPanel();
		jp3 = new JPanel();
		jp4 = new JPanel();
		jp5 = new JPanel();
		jp6 = new JPanel();
		newsFeed = new JPanel();
		
		jp1.setLayout(new BorderLayout(50, 50));
		jp1.setBackground(background);
		jp2.setBackground(background);
		jp3.setBackground(background);
		jp4.setBackground(background);
		jp5.setBackground(background);
		jp6.setBackground(background);
		
		newsFeed.setBackground(buttons);
		newsFeed.setLayout(new GridBagLayout());

		courses = new JLabel("News Feed");
		courses.setForeground(labels);
		courses.setFont(new Font(fontName, Font.BOLD, fontSize));
		courses.setBackground(labels);
		courses.setAlignmentX(Component.CENTER_ALIGNMENT);
		if (currentUser != null)
			username = new JLabel(currentUser.get_username());
		else
			username = new JLabel("Username");

		username.setForeground(labels);
		username.setBackground(labels);
		username.setFont(new Font(fontName, Font.BOLD, fontSize2));
		username.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		jsp = new JScrollPane(newsFeed, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		jsp.getViewport().add(newsFeed);
		jsp.setSize(200,jsp.getHeight());
		jsp.setBackground(buttons);
		newsFeed.setSize(200, jsp.getHeight());
		jsp.validate();

		logout = new JButton("Logout");
		logout.addActionListener(this);
		selectClass = new JButton("Go");
		selectClass.addActionListener(this);

//		Hashtable<String, Course> courseTable = currentUser.get_all_courses();
//		if (courseTable != null) {
//			for (String c : currentUser.get_all_courses().keySet()) {
//				userClassesTitles.add(c);
//
//			}
//			userBox.setSelectedIndex(0);
//		}

//		sample1 = new JButton("CSCI 270");
//		sample2 = new JButton("CSCI 201");
//		sample3 = new JButton("EE 101");
//		sample1.setBackground(buttons);
//		sample1.setOpaque(true);
//		sample1.setSize(100, 100);
//		sample1.setMinimumSize(new Dimension(150, 60));
//		sample1.setMaximumSize(new Dimension(150, 60));
//		sample1.setPreferredSize(new Dimension(150, 60));
//		sample2.setBackground(buttons);
//		sample2.setOpaque(true);
//		sample1.setSize(100, 100);
//		sample2.setMinimumSize(new Dimension(150, 60));
//		sample2.setMaximumSize(new Dimension(150, 60));
//		sample2.setPreferredSize(new Dimension(150, 60));
//		sample3.setBackground(buttons);
//		sample3.setOpaque(true);
//		sample3.setMinimumSize(new Dimension(150, 60));
//		sample3.setMaximumSize(new Dimension(150, 60));
//		sample3.setPreferredSize(new Dimension(150, 60));

		jp2.setLayout(new BorderLayout());
		jp4.add(courses);
		jp2.add(jp4, BorderLayout.NORTH);
		jp2.add(jsp, BorderLayout.CENTER);
		jp1.add(jp2, BorderLayout.CENTER);		

		userBox = new JComboBox<String>(userClassesTitles);
		
		if (currentUser.get_all_courses() != null
				&& currentUser.get_all_courses().size() > 0) {
			for (String c : currentUser.get_all_courses().keySet()) {
				userClassesTitles.add(c);
			}
			userBox.setSelectedIndex(0);
		}
		
		BoxLayout layout = new BoxLayout(jp6, BoxLayout.Y_AXIS);
		jp6.setLayout(layout);
		
		if (currentUser.get_all_courses() != null
				&& currentUser.get_all_courses().size() > 0) {
			for (String c : currentUser.get_all_courses().keySet()) {
				courseButtons.add(new JButton(c));
			}
		}
		
		for (int i=0; i<courseButtons.size(); i++) {
			jp6.add(Box.createRigidArea(new Dimension(0,25)));
			makeAction(courseButtons.get(i));
			jp6.add(courseButtons.get(i));
			courseButtons.get(i).setBackground(buttons);
			courseButtons.get(i).setOpaque(true);
			courseButtons.get(i).setSize(100, 100);
			courseButtons.get(i).setMinimumSize(new Dimension(150, 60));
			courseButtons.get(i).setMaximumSize(new Dimension(150, 60));
			courseButtons.get(i).setPreferredSize(new Dimension(150, 60));
			courseButtons.get(i).setAlignmentX(Component.CENTER_ALIGNMENT);
		}
		
		jp3.setLayout(new BorderLayout());
		jp5.add(username);
		jp5.add(userBox);
		jp5.add(selectClass);
		jp3.add(jp5, BorderLayout.NORTH);
		jp3.add(logout, BorderLayout.SOUTH);
		
		jp3.add(jp6, BorderLayout.CENTER);
		jp1.add(jp3, BorderLayout.EAST);

		add(jp1);
	}

	public void makeAction(final JButton button) {
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Course c = currentUser.get_single_course(button.getText());
				cp = new CoursePage(currentUser, s, oos, ois, c);
				setVisible(false);
				cp.setVisible(true);
				
			}
		});
	}
	
	

	private void setClose() {
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				try {
					Request r = new Request("close", "close");
					oos.writeObject(r); // tell server to close client's
					ois.close();
					oos.close();
					System.out.println("Closed streams");
				} catch (IOException e1) {
					System.out
							.println("IOE in LoginPage addListeners() window operation: "
									+ e1.getMessage());
				} finally {
					System.exit(0);
				}
			}
		});
	}

	public void actionPerformed(ActionEvent e) {
		// Need to fix this
		if (e.getSource() == selectClass) {
			selectedClass = (String) userBox.getSelectedItem();
			Course c = currentUser.get_single_course(selectedClass);

			cp = new CoursePage(currentUser, s, oos, ois, c);
			this.setVisible(false);
			cp.setVisible(true);

		}
		if (e.getSource() == logout) {
			LoginPage lp = new LoginPage();
			this.setVisible(false);
			lp.setVisible(true);
		}
	}

	/*
	 * 
	 * So once the announcement object exists you can add them like so:
	 * 
	 * 
	 * Object obj = ois.readObject(); Announcement ao = (Announcement) obj;
	 * ContentPaneManager.addContent(jp1, ao); jp1.validate();
	 * jsp.getViewport().add(jp1);
	 */
}
