package csci201_project;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 * HOW THIS WORKS:
 * 1. There are 5 buttons (announcements, syllabus, grades, assignments, lectureContent
 * 2. The action listeners are implemented from line 268 onward
 * 3. There is a contentPanel which is added to the main panel (jsp, line 233, the outermost panel, the one added to the frame)
 * 4. The outermost panel is has border layout. Content panel is added to the .CENTER
 * 5. There are 5 more panels (assignmentsPanel, announcementsPanel, gradesPanel, lectuerContentPanel, syllabusPanel)
 * 6. Each is has a grid bag layout, is added to a scrollpane
 * 7. Now, you just have to call the function in the ContentPaneManager class, pass in one of the five panels, and 
 *    the content object you want to add, and it should appear
*/
public class CoursePage extends JFrame implements ActionListener {
	
	JPanel jp1, jp2, jp3, jp4, jp5, jp6, topPanel, summaryPanel, jspPanel, jspPanel2;
	JLabel username, contentPaneExample, summary, className ;
	JButton logout, selectClass, announcements, makeAnnouncementButton, createGradeButton, syllabus, assignments, lectureContent, grades, back;
	List<JButton> summaryList = new ArrayList<JButton>();
	
	JComboBox<String> userBox;
	Vector<String> userClassesTitles = new Vector<String>();
	JScrollPane jsp, jsp2;
	
	private String fontName = "SANS_SERIF";
	private int fontSize = 48;
	private int fontSize2 = 24;
	
	Color labels;
	Color background;
	Color buttons;
	
	String selectedClass = "";
	private User currentUser;
	private Socket s;
	private ObjectOutputStream oos;
	private ObjectInputStream ois;
	private Course currentCourse;
	
	private JPanel contentPanel;
	
	private JPanel lectureContentPanel;
	private JPanel gradesPanel;
	private JPanel announcementsPanel;
	private JPanel makeAnnouncementsPanel;
	private InstructorCreateGrade createGradePanel;
	private JPanel assignmentsPanel;
	private JPanel syllabusPanel;
	
	private JScrollPane announcementsScrollPane;
	private JScrollPane makeAnnouncementsScrollPane;
	private JScrollPane createGradeScrollPane;
	private JScrollPane gradesScrollPane;
	private JScrollPane syllabusScrollPane;
	private JScrollPane lectureContentScrollPane;
	private JScrollPane assignmentsScrollPane;
	
	public CoursePage(User u, Socket s, ObjectOutputStream oos, ObjectInputStream ois, Course c) {
		super("SmartBoard: " + c.get_course_name());
		if(u instanceof Student)
			this.currentUser = (Student) u;
		else if (u instanceof Instructor){
			this.currentUser = (Instructor) u;
		}
		System.out.println(u.get_username());
		this.currentCourse = c;
    	setSize(1000,700);
    	setLocation(150,60);
    	setResizable(false);
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	
		background = new Color(255, 250, 240);
		labels = new Color(0, 0, 0);
		buttons = new Color(135, 206, 250);
		
		contentPanel = new JPanel();
		contentPanel.setLayout(new BorderLayout());
		
		lectureContentPanel = new JPanel();
		gradesPanel = new JPanel();
		announcementsPanel = new JPanel();
		makeAnnouncementsPanel = new InstructorAnnouncement(currentCourse, s, oos);
		assignmentsPanel = new JPanel();
		syllabusPanel = new JPanel();
		
		createGradePanel = new InstructorCreateGrade(currentCourse, s, oos);
		
		
		announcementsPanel.setLayout(new GridBagLayout());
		announcementsScrollPane = new JScrollPane(announcementsPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		announcementsPanel.setBackground(Color.blue);
		
		
		makeAnnouncementsScrollPane = new JScrollPane(makeAnnouncementsPanel, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		makeAnnouncementsPanel.setBackground(buttons);
		
		createGradeScrollPane = new JScrollPane(createGradePanel, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		
		lectureContentPanel.setLayout(new GridBagLayout());
		lectureContentScrollPane = new JScrollPane(lectureContentPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		lectureContentPanel.setBackground(Color.red);
		
		gradesPanel.setLayout(new GridBagLayout());
		gradesScrollPane = new JScrollPane(gradesPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		gradesPanel.setBackground(Color.green);
		
		assignmentsPanel.setLayout(new GridBagLayout());
		assignmentsScrollPane = new JScrollPane(assignmentsPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		assignmentsPanel.setBackground(Color.yellow);
		
		syllabusPanel.setLayout(new GridBagLayout());
		syllabusScrollPane = new JScrollPane(syllabusPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
/*   Haven't 100% gotten how to get the PDF viewer open should work for anything though.
 * 	 This will probably be a fun bug fix
 * if (Desktop.isDesktopSupported()) {
    try {
        File myFile = new File("/path/to/file.pdf");
        Desktop.getDesktop().open(myFile);
    } catch (IOException ex) {
        // no application registered for PDFs
    }
}
 * 
 * 
 * 
 */
		syllabusPanel.setBackground(Color.black);
		
		announcementsScrollPane.getViewport().add(announcementsPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		announcementsPanel.validate();
		
		makeAnnouncementsScrollPane.getViewport().add(makeAnnouncementsPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		makeAnnouncementsPanel.validate();
		
		createGradeScrollPane.getViewport().add(createGradePanel, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		createGradePanel.validate();
		
		lectureContentScrollPane.getViewport().add(lectureContentPanel);
		lectureContentPanel.validate();
		
		gradesScrollPane.getViewport().add(gradesPanel);
		gradesPanel.validate();
		
		assignmentsScrollPane.getViewport().add(assignmentsPanel);
		assignmentsPanel.validate();
		
		syllabusScrollPane.getViewport().add(syllabusPanel);
		syllabusPanel.validate();
		
		contentPanel.add(announcementsScrollPane, BorderLayout.CENTER);
		contentPanel.validate();
    	
    	jp1 = new JPanel();
    	jp2 = new JPanel();
    	jp3 = new JPanel();
    	jp4 = new JPanel();
    	jp5 = new JPanel();
    	jp6 = new JPanel();
    	topPanel = new JPanel();
    	summaryPanel = new JPanel();
    	jspPanel = new JPanel();
    	jspPanel2 = new JPanel();
    	jsp = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    	jsp2 = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    	
    	jp1.setLayout(new BorderLayout(50,50));
    	jp1.setBackground(background);
    	jp2.setBackground(background);
    	jp3.setBackground(background);
    	jp4.setBackground(background);
    	jp5.setBackground(background);
    	topPanel.setBackground(background);
    	
    	className = new JLabel("Content Pane");
    	className.setForeground(labels);
    	className.setFont(new Font(fontName, Font.BOLD, fontSize));
    	className.setBackground(labels);
    	className.setAlignmentX(Component.CENTER_ALIGNMENT);
    	if(currentUser != null){
    		username = new JLabel(currentUser.get_username());
    	}else{
    		username = new JLabel("Username");
    	}
    	username.setForeground(labels);
    	username.setBackground(labels);
    	username.setFont(new Font(fontName, Font.BOLD, fontSize2));
    	
    	back = new JButton("Back");
    	back.addActionListener(this);
    	
    	contentPaneExample = new JLabel("Content Pane");
    	logout = new JButton("Logout");
    	logout.addActionListener(this);
    	selectClass = new JButton("Go");
    	summary = new JLabel("Summary");
    	summary.setFont(new Font(fontName, Font.BOLD, fontSize));
    	
    	createGradeButton = new JButton("Create Grade");
    	createGradeButton.setFont(new Font(fontName, Font.BOLD, fontSize2));
    	createGradeButton.setAlignmentX(Component.CENTER_ALIGNMENT);
    	createGradeButton.addActionListener(this);
    	
    	makeAnnouncementButton = new JButton("Make Announcement");
    	makeAnnouncementButton.setFont(new Font(fontName, Font.BOLD, fontSize2));
    	makeAnnouncementButton.setAlignmentX(Component.CENTER_ALIGNMENT);
    	makeAnnouncementButton.addActionListener(this);
    	
    	announcements = new JButton("Announcements");
    	announcements.setFont(new Font(fontName, Font.BOLD, fontSize2));
    	announcements.setAlignmentX(Component.CENTER_ALIGNMENT);
    	announcements.addActionListener(this);
    	syllabus = new JButton("Syllabus");
    	syllabus.setFont(new Font(fontName, Font.BOLD, fontSize2));
    	syllabus.setAlignmentX(Component.CENTER_ALIGNMENT);
    	syllabus.addActionListener(this);
    	assignments = new JButton("Assignments");
    	assignments.setFont(new Font(fontName, Font.BOLD, fontSize2));
    	assignments.setAlignmentX(Component.CENTER_ALIGNMENT);
    	assignments.addActionListener(this);
    	lectureContent = new JButton("Lecture Content");
    	lectureContent.setFont(new Font(fontName, Font.BOLD, fontSize2));
    	lectureContent.setAlignmentX(Component.CENTER_ALIGNMENT);
    	lectureContent.addActionListener(this);
    	grades = new JButton("Grades");
    	grades.setFont(new Font(fontName, Font.BOLD, fontSize2));
    	grades.setAlignmentX(Component.CENTER_ALIGNMENT);
    	grades.addActionListener(this);
    	
    	summaryList.add(announcements);
    	if(currentUser instanceof Instructor){
    		summaryList.add(makeAnnouncementButton);
    		summaryList.add(createGradeButton);
    	}
    	summaryList.add(syllabus);
    	summaryList.add(assignments);
    	summaryList.add(lectureContent);
    	summaryList.add(grades);
    	jspPanel2.add(announcements);
    	if(currentUser instanceof Instructor){
    		jspPanel2.add(makeAnnouncementButton);
    		jspPanel2.add(createGradeButton);
    	}
    	jspPanel2.add(syllabus);
    	jspPanel2.add(assignments);
    	jspPanel2.add(lectureContent);
    	jspPanel2.add(grades);
    	
    	jspPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 5));
    	jspPanel2.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));

    	jsp.setBackground(buttons);
    	jspPanel.setBackground(buttons);
    	jspPanel2.setLayout(new BoxLayout((Container)jspPanel2, BoxLayout.Y_AXIS));
    	jsp2.setViewportView(jspPanel2);
    	jsp2.setBackground(buttons);
    	jspPanel2.setBackground(buttons);
    	
    	jp2.setLayout(new BorderLayout());
    	topPanel.setLayout(new BorderLayout());
    	topPanel.add(className, BorderLayout.CENTER);
    	topPanel.add(back, BorderLayout.WEST);
    	jp2.add(topPanel, BorderLayout.NORTH);
    	jp2.add(jsp, BorderLayout.CENTER);
    	jp1.add(contentPanel, BorderLayout.CENTER);
    	
    	userBox = new JComboBox<String>(userClassesTitles);
    	if(currentUser instanceof Student){
	    	if (((Student) currentUser).get_all_courses() != null
					&& ((Student) currentUser).get_all_courses().size() > 0) {
				for (String currCourse : ((Student) currentUser).get_all_courses().keySet()){
					userClassesTitles.add(currCourse);
				}
				userBox.setSelectedIndex(0);
			}
    	} else if(currentUser instanceof Instructor){
    		if (((Instructor) currentUser).get_all_courses() != null
					&& ((Instructor) currentUser).get_all_courses().size() > 0) {
				for (String currCourse : ((Instructor) currentUser).get_all_courses().keySet()){
					userClassesTitles.add(currCourse);
				}
				userBox.setSelectedIndex(0);
			}
    	}
		jp3.setLayout(new BorderLayout());
		jp6.setLayout(new FlowLayout());
		jp5.add(username);
		jp5.add(userBox);
		jp5.add(selectClass);
		jp3.add(jp5, BorderLayout.NORTH);
		jp6.add(back);
		jp6.add(logout);
		jp3.add(jp6, BorderLayout.SOUTH);
		jp1.add(jp3, BorderLayout.EAST);
		selectClass.addActionListener(this);
		summaryPanel.setLayout(new FlowLayout());
		summaryPanel.add(summary);
		jp3.add(summaryPanel, BorderLayout.CENTER);
		jp3.add(jsp2, BorderLayout.CENTER);
		
		add(jp1);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == logout) {
			LoginPage lp = new LoginPage();
			this.setVisible(false);
			lp.setVisible(true);
		} else if (e.getSource() == selectClass) {
			selectedClass = (String) userBox.getSelectedItem();
			Course c = null;
			if(currentUser instanceof Instructor)
				c = ((Instructor) currentUser).get_single_course(selectedClass);
			else if(currentUser instanceof Student) 
				c = ((Student) currentUser).get_single_course(selectedClass);

			CoursePage cp = new CoursePage(currentUser, s, oos, ois, c);
			this.setVisible(false);
			cp.setVisible(true);
		} else if (e.getSource() == back) {
			if(currentUser instanceof Student){
				StudentHomePage shp = new StudentHomePage(currentUser, s, oos, ois, null);
				this.setVisible(false);
				shp.setVisible(true);
			} else {
				InstructorHomePage ihp = new InstructorHomePage(currentUser, s, oos, ois);
				this.setVisible(false);
				ihp.setVisible(true);
			}
		} else if (e.getSource() == announcements) {
			contentPanel.removeAll();
			contentPanel.add(announcementsScrollPane, BorderLayout.CENTER);
			contentPanel.validate();
			contentPanel.repaint();
		} else if (e.getSource() == syllabus) {
			contentPanel.removeAll();
			contentPanel.add(syllabusScrollPane, BorderLayout.CENTER);
			contentPanel.validate();
			contentPanel.repaint();
		} else if (e.getSource() == assignments) {
			contentPanel.removeAll();
			contentPanel.add(assignmentsScrollPane, BorderLayout.CENTER);
			contentPanel.validate();
			contentPanel.repaint();
		} else if (e.getSource() == grades) {
			contentPanel.removeAll();
			contentPanel.add(gradesScrollPane, BorderLayout.CENTER);
			contentPanel.validate();
			contentPanel.repaint();
		} else if (e.getSource() == lectureContent) {
			contentPanel.removeAll();
			contentPanel.add(lectureContentScrollPane, BorderLayout.CENTER);
			contentPanel.validate();
			contentPanel.repaint();
		} else if(e.getSource() == makeAnnouncementButton){
			contentPanel.removeAll();
			contentPanel.add(makeAnnouncementsScrollPane, BorderLayout.CENTER);
			contentPanel.validate();
			contentPanel.repaint();
		} else if(e.getSource() == createGradeButton){
			contentPanel.removeAll();
			contentPanel.add(createGradeScrollPane, BorderLayout.CENTER);
			contentPanel.validate();
			contentPanel.repaint();
		}
	}
}
