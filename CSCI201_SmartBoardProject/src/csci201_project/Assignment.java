package csci201_project;


public class Assignment extends ContentObject {
	
	//MEMBER VARIABLES//
	///////////////////
	
	private String assignmentTitle;
	private String assignmentDate;
	private String assignmentComment;
	private String assignmentDownloadLabel;
	
	//CONSTRUCTOR//
	//////////////

	public Assignment(String assignmentTitle, String assignmentDate, String assignmentComment, String assignmentDownloadLabel) {
		super(assignmentTitle, assignmentDate, assignmentComment, assignmentDownloadLabel);
		
		this.assignmentTitle = assignmentTitle;
		this.assignmentDate = assignmentDate;
		this.assignmentComment = assignmentComment;
		this.assignmentDownloadLabel = assignmentDownloadLabel;
	}
	
	//SETTER FUNCTIONS//
	///////////////////
	
	//SET ASSIGNMENT TITLE
	public void setAssignmentTitle(String assignmentTitle) {
		this.assignmentTitle = assignmentTitle;
	}
	
	//SET ASSIGNMENT DATE 
	public void setAssignmentDate(String assignmentDate) {
		this.assignmentDate = assignmentDate;
	}
	
	//SET ASSIGNMENT COMMENT
	public void setAssignmentComment(String assignmentComment) {
		this.assignmentComment = assignmentComment;
	}
	
	//SET ASSIGNMENT DOWNLOAD LABEL
	public void setAssignmentCourse(String assignmentDownloadLabel) {
		this.assignmentDownloadLabel = assignmentDownloadLabel;
	}
	
	//GETTER FUNCTIONS//
	///////////////////
	
	//GET ASSIGNMENT TITLE
	public String getAssignmentTitle() {
		return this.assignmentTitle;
	}
	
	//GET ASSIGNMENT DATE
	public String getAssignmentDate() {
		return this.assignmentDate;
	}
	
	//GET ASSIGNMENT COMMENT
	public String getAssignmentComment() {
		return this.assignmentComment;
	}
	
	//GET ASSIGNMENT DOWNLOAD LABEL
	public String getAssignmentDownloadLabel() {
		return this.assignmentDownloadLabel;
	}

}
