package csci201_project;


import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.MessageDigest;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;


/*
 * Found through online resource. Edited very slightly to apply here
 * Came from howtodoinjava.com:
 * http://howtodoinjava.com/2013/07/22/how-to-generate-secure-password-hash-md5-sha-pbkdf2-bcrypt-examples/
 * 
 */
public class HashPassword {
		
		//public String  hashAPassword (String pwd) throws NoSuchAlgorithmException {
		//	String securedPassword = getSecurePassword(pwd, salt);
		//	return securedPassword;
		//}
		
		public static String getSecurePassword(String passwordToHash, String salt)
	    {
	        String generatedPassword = null;
	        try {
	            // Create MessageDigest instance for MD5
	            MessageDigest md = MessageDigest.getInstance("MD5");
	            //Add password bytes to digest
	            md.update(salt.getBytes());
	            //Get the hash's bytes 
	            byte[] bytes = md.digest(passwordToHash.getBytes());
	            //This bytes[] has bytes in decimal format;
	            //Convert it to hexadecimal format
	            StringBuilder sb = new StringBuilder();
	            for(int i=0; i< bytes.length ;i++)
	            {
	                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
	            }
	            //Get complete hashed password in hex format
	            generatedPassword = sb.toString();
	        } 
	        catch (NoSuchAlgorithmException e) {
	            e.printStackTrace();
	        }
	        return generatedPassword;
	    }
		
		public static String getSalt() throws NoSuchAlgorithmException
		{
		    //Always use a SecureRandom generator
		    SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
		    //Create array for salt
		    byte[] salt = new byte[16];
		    //Get a random salt
		    sr.nextBytes(salt);
		    //return salt
		    return salt.toString();
		}
}


