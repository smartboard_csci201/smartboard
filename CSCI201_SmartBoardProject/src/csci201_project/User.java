package csci201_project;

import java.io.Serializable;

public abstract class User implements Serializable{
	private static final long serialVersionUID = 1L;

	protected int id; 
	protected String username; 

	//methods: 

	public User (int id, String username) {
		this.id = id;
		this.username = username;
	}

	public abstract void openHomePage();

	public int get_id() {
		return id;
	}

	public String get_username() {
		return username;
	}

}
