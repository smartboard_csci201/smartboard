package csci201_project;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

import javax.swing.JFrame;

public class ListenerThread extends Thread {
	// Pages in this thread
	private LoginPage loginPage;

	private Socket s;
	private ObjectInputStream ois;
	private LoginPage page;

	public ListenerThread(LoginPage page, Socket s, ObjectInputStream ois) {
		this.page = page;
		this.s = s;
		this.ois = ois;
		System.out.println("New Listener Thread on " +  page.getName() + "!!!");

	}


	public void run() {
		try {
			while (true) {
				Object obj = ois.readObject();
				Request r = (Request) obj;
				if(r.getRequestType().equals("close this listener thread")){
					System.out.println("Close listener for " + page.getName());
					break;
				}
				System.out.println("Accept request on " + page.getName());
				page.acceptRequest(r);
//				if (r.getRequestType().equals("user login")) {
//					User u = (User) r.getObj();
//					loginPage.getUserFromListenerThread(u);
//				} else {
//					if(hp != null){
//						System.out.println("HP NOT NULL");
//						hp.acceptRequest(r);
//					}
//				}
			}

		} catch (IOException ioe) {
			System.out.println("IOE in ListenerThread.run(): "
					+ ioe.getMessage());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
