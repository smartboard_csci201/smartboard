package csci201_project;
///*
// * Announcement Class
// * - Teacher/Admin makes announcement
// * - Server takes announcement and notifies all clients
// * - Newsfeed only updates if User has that course (or if admin makes announcement, all get)
// * */
//public class Announcement {
//	private Course course;
//	private String message;
//	public Announcement(Course course, String message){
//		this.course = course;
//		this.message = message;
//	}
//	
//	public String getMessage(){
//		return message;
//	}
//	
//	public Course getCourse(){
//		return course;
//	}
//}


public class Announcement extends ContentObject {
	
	//MEMBER VARIABLES//
	///////////////////


	private String announcementTitle;
	private String announcementDate;
	private String announcementComment;
	private String announcementCourse;
	private String announcementCategory;
	
	private Course course;
	private String message;
	
	//CONSTRUCTOR//
	//////////////
	
	
	
	public Announcement(String announcementTitle, String announcementDate, String announcementComment, String announcementCourse, String announcementCategory) {
		super(announcementTitle, announcementDate, announcementComment, announcementCourse + ": " + announcementCategory);
		
		this.announcementTitle = announcementTitle;
		this.announcementDate = announcementDate;
		this.announcementComment = announcementComment;
		this.announcementCourse = announcementCourse;
		this.announcementCategory = announcementCategory;
	}
	
	public Announcement(Course course, String message){
		super(course, message);
	}
	
	//SETTER FUNCTIONS//
	///////////////////
	
	//SET ANNOUNCEMENT TITLE
	public void setAnnouncementTitle(String announcementTitle) {
		this.announcementTitle = announcementTitle;
	}
	
	//SET ANNOUNCEMENT DATE
	public void setAnnouncementDate(String announcementDate) {
		this.announcementDate = announcementDate;
	}
	
	//SET ANNOUNCEMENT COMMENT
	public void setAnnouncementComment(String announcementComment) {
		this.announcementComment = announcementComment;
	}
	
	//SET ANNOUNCEMENT COURSE
	public void setAnnouncementCourse(String announcementCourse) {
		this.announcementCourse = announcementCourse;
	}
	
	//SET ANNOUNCEMENT CATEGORY
	public void setAnnouncementCategory(String announcementCategory) {
		this.announcementCategory = announcementCategory;
	}
	
	//GETTER FUNCTIONS//
	///////////////////
	
	//GET ANNOUNCEMENT TITLE
	public String getAnnouncementTitle() {
		return this.announcementTitle;
	}
	
	//GET ANNOUNCEMENT DATE
	public String getAnnouncementDate() {
		return this.announcementDate;
	}
	
	//GET ANNOUNCEMENT COMMENT
	public String getAnnouncementComment() {
		return this.announcementComment;
	}
	
	//GET ANNOUNCEMENT COURSE
	public String getAnnouncementCourse() {
		return this.announcementCourse;
	}
	
	//GET ANNOUNCEMENT CATEGORY
	public String getAnnouncementCategory() {
		return this.announcementCategory;
	}

}


