package csci201_project;


public class LectureContent extends ContentObject {
	
	//MEMBER VARIABLES//
	///////////////////
	
	private String lectureContentTitle;
	private String lectureContentDate;
	private String lectureContentComment;
	private String lectureContentDownloadLabel;
	
	//CONSTRUCTOR//
	//////////////

	public LectureContent(String lectureContentTitle, String lectureContentDate, String lectureContentComment, String lectureContentDownloadLabel) {
		super(lectureContentTitle, lectureContentDate, lectureContentComment, lectureContentDownloadLabel);
		
		this.lectureContentTitle = lectureContentTitle;
		this.lectureContentDate = lectureContentDate;
		this.lectureContentComment = lectureContentComment;
		this.lectureContentDownloadLabel = lectureContentDownloadLabel;
	}
	
	//SETTER FUNCTIONS//
	///////////////////
	
	//SET LECTURE CONTENT TITLE
	public void setLectureContentTitle(String lectureContentTitle) {
		this.lectureContentTitle = lectureContentTitle;
	}
	
	//SET LECTURE CONTENT DATE 
	public void setLectureContentDate(String lectureContentDate) {
		this.lectureContentDate = lectureContentDate;
	}
	
	//SET LECTURE CONTENT COMMENT
	public void setLectureContentComment(String lectureContentComment) {
		this.lectureContentComment = lectureContentComment;
	}
	
	//SET LECTURE CONTENT DOWNLOAD LABEL
	public void setLectureContentDownloadLabel(String lectureContentDownloadLabel) {
		this.lectureContentDownloadLabel = lectureContentDownloadLabel;
	}
	
	//GETTER FUNCTIONS//
	///////////////////
	
	//GET LECTURE CONTENT TITLE
	public String getLectureContentTitle() {
		return this.lectureContentTitle;
	}
	
	//GET LECTURE CONTENT DATE
	public String getLectureContentDate() {
		return this.lectureContentDate;
	}
	
	//GET LECTURE CONTENT COMMENT
	public String getLectureContentComment() {
		return this.lectureContentComment;
	}
	
	//GET LECTURE CONTENT DOWNLOAD LABEL
	public String getLectureContentDownloadLabel() {
		return this.lectureContentDownloadLabel;
	}

}
