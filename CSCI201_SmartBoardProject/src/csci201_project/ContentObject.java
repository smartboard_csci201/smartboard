package csci201_project;

import java.io.Serializable;

public abstract class ContentObject implements Serializable{

	//MEMBER VARIABLES//
	///////////////////
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String contentObjectTitle;
	String contentObjectDate;
	String contentObjectComment;
	String contentObjectCustomInfo;
	
	//CONSTRUCTOR//
	//////////////
	
	public ContentObject(String contentObjectTitle, String contentObjectDate, String contentObjectComment, String contentObjectCustomInfo) {
		this.contentObjectTitle = contentObjectTitle;
		this.contentObjectDate = contentObjectDate;
		this.contentObjectComment = contentObjectComment;
		this.contentObjectCustomInfo = contentObjectCustomInfo;
	}
	
	public ContentObject(Course course, String message ) {
		this.contentObjectTitle = course.get_course_name();
		this.contentObjectComment = message;
	}
	
	//SETTER FUNCTIONS//
	///////////////////
	
	//SET CONTENT OBJECT TITLE
	public void setContentObjectTitle(String contentObjectTitle) {
		this.contentObjectTitle = contentObjectTitle;
	}
	
	//SET CONTENT OBJECT DATE
	public void setContentObjectDate(String contentObjectDate) {
		this.contentObjectDate = contentObjectDate;
	}
	
	//SET CONTENT OBJECT COMMENT
	public void setContentObjectComment(String contentObjectComment) {
		this.contentObjectComment = contentObjectComment;
	}
	
	//SET CONTENT OBJECT CUSTOM INFO
	public void setContentObjectCustomInfo(String contentObjectCustomInfo) {
		this.contentObjectCustomInfo = contentObjectCustomInfo;
	}
	
	//GETTER FUNCTIONS//
	///////////////////
	
	//GET CONTENT OBJECT TITLE
	public String getContentObjectTitle() {
		return this.contentObjectTitle;
	}
	
	//GET CONTENT OBJECT DATE
	public String getContentObjectDate() {
		return this.contentObjectDate;
	}
	
	//GET CONTENT OBJECT COMMENT
	public String getContentObjectComment() {	
		return this.contentObjectComment;	
	}
	
	//GET CONTENT OBJECT CUSTOM INFO
	public String getContentObjectCustomInfo() {
		return this.contentObjectCustomInfo;	
	}
	
}
