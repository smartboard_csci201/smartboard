package csci201_project;
//
//import java.awt.Color;
//import java.awt.GridBagConstraints;
//import java.awt.GridBagLayout;
//
//import javax.swing.BorderFactory;
//import javax.swing.JLabel;
//import javax.swing.JPanel;
//import javax.swing.JScrollPane;
//import javax.swing.JTextArea;
//
//public abstract class ContentBox extends JPanel {
//	
//	private static final long serialVersionUID = 1L;
//	private String title, datePosted, comment, custom;
//	private JLabel titleLabel, timePostedLabel, customLabel;
//	private JPanel titlePanel, commentPanel;
//	private JTextArea commentTA;
//	
//	public ContentBox(String title, String datePosted, String comment, String custom) {
//		
//		this.setLayout(new GridBagLayout());
//		
//		this.title = title;
//		this.datePosted = datePosted;
//		this.comment = comment;
//		this.custom = custom;
//		
//		GridBagConstraints gbc = new GridBagConstraints();
//		
//		timePostedLabel = new JLabel(this.datePosted);
//		titleLabel = new JLabel(this.title);
//		
//		customLabel = new JLabel(this.custom);
//		
//		commentTA = new JTextArea(5, 40);
//		JScrollPane scrollPane = new JScrollPane(commentTA); 
//		commentTA.setEditable(false);
//		commentTA.setLineWrap(true);
//		commentTA.setWrapStyleWord(true);
//		commentTA.setBackground(Color.LIGHT_GRAY);
//		commentTA.setText(comment);
//		
//		titlePanel = new JPanel();
//		titlePanel.setLayout(new GridBagLayout());
//		commentPanel = new JPanel();
//		
//		gbc.gridx = 0;
//		gbc.gridy = 0;
//		gbc.ipadx = 100;
//		titlePanel.add(customLabel, gbc);
//		
//		gbc.gridx = 1;
//		gbc.gridy = 0;
//		gbc.ipadx = 75;
//		titlePanel.add(titleLabel, gbc);
//		
//		gbc.gridx = 2;
//		gbc.gridy = 0;
//		gbc.ipadx = 50;
//		titlePanel.add(timePostedLabel, gbc);
//		
//		gbc.gridx = 0;
//		gbc.gridy = 0;
//		gbc.ipadx = 0;
//		gbc.ipady = 0;
//		this.add(titlePanel, gbc);
//		
//		commentPanel.add(commentTA);
//	
//		gbc.gridx = 0;
//		gbc.gridy = 1;
//		gbc.ipadx = 0;
//		gbc.ipady = 50;
//		this.add(commentPanel, gbc);
//		
//		this.setSize(200, 100);
//		this.setBorder(BorderFactory.createLineBorder(Color.black));
//	}
//	
//	public void setTitle(String title) {
//		
//		this.title = title;
//		
//	}
//	
//	public void setTimePosted(String timePosted) {
//		
//		this.datePosted = timePosted;
//		
//	}
//	
//	public void setComment(String comment) {
//		
//		this.comment = comment;
//		
//	}
//	
//	public void setCustoM(String custom) {
//		
//		this.custom = custom;
//		
//	}
//	
//	public String getTitle() {
//		
//		return this.title;
//		
//	}
//	
//	public String getTimePosted() {
//		
//		return this.datePosted;
//		
//	}
//	
//	public String getComment() {
//		
//		return this.comment;
//		
//	}
//	
//	public String getCustom() {
//		
//		return this.custom;
//		
//	}
//	
//}

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.Serializable;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public abstract class ContentBox extends JPanel implements Serializable{
	
	//MEMBER VARIABLES//
	///////////////////
	
	private static final long serialVersionUID = 1L;
	
	
	//INFORMATION STORED IN STRINGS
	private String contentBoxTitle;
	private String contentBoxDate;
	private String contentBoxComment;
	private String contentBoxCustomInfo;
	
	//CONSTRUCTOR//
	//////////////
	
	public ContentBox(ContentObject co) {
		this.contentBoxTitle = co.getContentObjectTitle();
		this.contentBoxDate = co.getContentObjectDate();
		this.contentBoxComment = co.getContentObjectComment();
		this.contentBoxCustomInfo = co.getContentObjectCustomInfo();
		
		if(co instanceof Announcement) {
			createAnnouncementBox();
		} else if(co instanceof Assignment) {
			createAssignmentBox();
		} else if(co instanceof Grade) {
			createGradeBox();
		} else if(co instanceof LectureContent) {
			createLectureContentBox();
		}
	}
	
	//CREATES GUI FOR CONTENT BOX//
	//////////////////////////////
	
	private void createAnnouncementBox() {
		this.setLayout(new GridBagLayout());
		
		GridBagConstraints gbc = new GridBagConstraints();
		
		JLabel announcementDateLabel = new JLabel(this.contentBoxDate);
		JLabel announcementTitleLabel = new JLabel(this.contentBoxTitle);
		JLabel announcementCustomLabel = new JLabel(this.contentBoxCustomInfo);
		
		JTextArea commentTA = new JTextArea(5, 40);
		JScrollPane scrollArea = new JScrollPane(commentTA); 
		commentTA.setEditable(false);
		commentTA.setLineWrap(true);
		commentTA.setWrapStyleWord(true);
		commentTA.setBackground(Color.LIGHT_GRAY);
		commentTA.setText(this.contentBoxComment);
		
		JPanel headerPanel = new JPanel();
		headerPanel.setLayout(new GridBagLayout());
		
		JPanel commentAreaPanel = new JPanel();
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.ipadx = 100;
		headerPanel.add(announcementCustomLabel, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.ipadx = 75;
		headerPanel.add(announcementTitleLabel, gbc);
		
		gbc.gridx = 2;
		gbc.gridy = 0;
		gbc.ipadx = 50;
		headerPanel.add(announcementDateLabel, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.ipadx = 0;
		gbc.ipady = 0;
		this.add(headerPanel, gbc);
		
		commentAreaPanel.add(scrollArea);
	
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.ipadx = 0;
		gbc.ipady = 50;
		this.add(commentAreaPanel, gbc);
		
		this.setSize(200, 100);
		this.setBorder(BorderFactory.createLineBorder(Color.black));
		this.setVisible(true);
	}
	
	private void createAssignmentBox() {
		this.setLayout(new GridBagLayout());
		
		GridBagConstraints gbc = new GridBagConstraints();
		
		JLabel assignmentDateLabel = new JLabel(this.contentBoxDate);
		JLabel assignmentTitleLabel = new JLabel(this.contentBoxTitle);
		JLabel assignmentCustomLabel = new JLabel(this.contentBoxCustomInfo);
		
		JTextArea commentTA = new JTextArea(5, 40);
		JScrollPane scrollArea = new JScrollPane(commentTA); 
		commentTA.setEditable(false);
		commentTA.setLineWrap(true);
		commentTA.setWrapStyleWord(true);
		commentTA.setBackground(Color.LIGHT_GRAY);
		commentTA.setText(this.contentBoxComment);
		
		JPanel headerPanel = new JPanel();
		headerPanel.setLayout(new GridBagLayout());
		
		JPanel commentAreaPanel = new JPanel();
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.ipadx = 100;
		headerPanel.add(assignmentDateLabel, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.ipadx = 75;
		headerPanel.add(assignmentTitleLabel, gbc);
		
		gbc.gridx = 2;
		gbc.gridy = 0;
		gbc.ipadx = 50;
		headerPanel.add(assignmentCustomLabel, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.ipadx = 0;
		gbc.ipady = 0;
		this.add(headerPanel, gbc);
		
		commentAreaPanel.add(scrollArea);
	
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.ipadx = 0;
		gbc.ipady = 50;
		this.add(commentAreaPanel, gbc);
		
		this.setSize(200, 100);
		this.setBorder(BorderFactory.createLineBorder(Color.black));
	}
	
	private void createGradeBox() {
		this.setLayout(new GridBagLayout());
		
		GridBagConstraints gbc = new GridBagConstraints();
		
		JLabel gradeDateLabel = new JLabel(this.contentBoxDate);
		JLabel gradeTitleLabel = new JLabel(this.contentBoxTitle);
		JLabel gradeCustomLabel = new JLabel(this.contentBoxCustomInfo);
		
		JTextArea commentTA = new JTextArea(5, 40);
		JScrollPane scrollArea = new JScrollPane(commentTA); 
		commentTA.setEditable(false);
		commentTA.setLineWrap(true);
		commentTA.setWrapStyleWord(true);
		commentTA.setBackground(Color.LIGHT_GRAY);
		commentTA.setText(this.contentBoxComment);
		
		JPanel headerPanel = new JPanel();
		headerPanel.setLayout(new GridBagLayout());
		
		JPanel commentAreaPanel = new JPanel();
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.ipadx = 100;
		headerPanel.add(gradeTitleLabel, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.ipadx = 75;
		headerPanel.add(gradeDateLabel, gbc);
		
		gbc.gridx = 2;
		gbc.gridy = 0;
		gbc.ipadx = 50;
		headerPanel.add(gradeCustomLabel, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.ipadx = 0;
		gbc.ipady = 0;
		this.add(headerPanel, gbc);
		
		commentAreaPanel.add(scrollArea);
	
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.ipadx = 0;
		gbc.ipady = 50;
		this.add(commentAreaPanel, gbc);
		
		this.setSize(200, 100);
		this.setBorder(BorderFactory.createLineBorder(Color.black));
	}
	
	private void createLectureContentBox() {
		this.setLayout(new GridBagLayout());
		
		GridBagConstraints gbc = new GridBagConstraints();
		
		JLabel lectureContentDateLabel = new JLabel(this.contentBoxDate);
		JLabel lectureContentTitleLabel = new JLabel(this.contentBoxTitle);
		JLabel lectureContentCustomLabel = new JLabel(this.contentBoxCustomInfo);
		
		JTextArea commentTA = new JTextArea(5, 40);
		JScrollPane scrollArea = new JScrollPane(commentTA); 
		commentTA.setEditable(false);
		commentTA.setLineWrap(true);
		commentTA.setWrapStyleWord(true);
		commentTA.setBackground(Color.LIGHT_GRAY);
		commentTA.setText(this.contentBoxComment);
		
		JPanel headerPanel = new JPanel();
		headerPanel.setLayout(new GridBagLayout());
		
		JPanel commentAreaPanel = new JPanel();
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.ipadx = 100;
		headerPanel.add(lectureContentDateLabel, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.ipadx = 75;
		headerPanel.add(lectureContentTitleLabel, gbc);
		
		gbc.gridx = 2;
		gbc.gridy = 0;
		gbc.ipadx = 50;
		headerPanel.add(lectureContentCustomLabel, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.ipadx = 0;
		gbc.ipady = 0;
		this.add(headerPanel, gbc);
		
		commentAreaPanel.add(scrollArea);
	
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.ipadx = 0;
		gbc.ipady = 50;
		this.add(commentAreaPanel, gbc);
		
		this.setSize(200, 100);
		this.setBorder(BorderFactory.createLineBorder(Color.black));
	}
	
	//SETTER FUNCTIONS//
	///////////////////
	
	//SET CONTENT BOX TITLE
	public void setContentBoxTitle(String contentBoxTitle) {
		this.contentBoxTitle = contentBoxTitle;
	}
	
	//SET CONTENT BOX DATE
	public void setContentBoxDate(String contentBoxDate) {
		this.contentBoxDate = contentBoxDate;
	}
	
	//SET CONTENT BOX COMMENT
	public void setContentBoxComment(String contentBoxComment) {
		this.contentBoxComment = contentBoxComment;
	}
	
	//SET CONTENT BOX CUSTOM INFO
	public void setContentBoxCustomInfo(String contentBoxCustomInfo) {
		this.contentBoxCustomInfo = contentBoxCustomInfo;
	}
	
	//GETTER FUNCTIONS//
	///////////////////
	
	//GET CONTENT BOX TITLE
	public String getContentBoxTitle() {
		return this.contentBoxTitle;
	}
	
	//GET CONTENT BOX DATE
	public String getContentBoxDate() {
		return this.contentBoxDate;
	}
	
	//GET CONTENT BOX COMMENT
	public String getContentBoxComment() {	
		return this.contentBoxComment;	
	}
	
	//GET CONTENT BOX CUSTOM INFO
	public String getContentBoxCustomInfo() {
		return this.contentBoxCustomInfo;	
	}
	
}
