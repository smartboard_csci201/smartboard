package csci201_project;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class AdminAddToCourse extends JFrame implements ActionListener {
	/**
	 * AdminAddToCourse is a JFrame where the admin can add Students or
	 * Instructors to a course
	 * 
	 * Created by: Robyn
	 */
	// Objects
	private JPanel topPanel, middlePanel, bottomPanel;
	private JLabel courseTitleLabel, addLabel, pageTitleLabel, chooseUserLabel;
	private JComboBox courseComboBox, userComboBox;
	private JRadioButton studentRad, instructorRad;
	private JButton createButton, backButton;
	private ButtonGroup buttonGroup;

	// User variables
	private Vector<User> userVector;
	private Vector<Course> courseVector;

	// Formatting
	private String fontName = "SANS_SERIF";
	private int fontSize = 48;
	private int fontSize2 = 24;
	private Color darkBlue, lightBlue, pink;

	// Stream vars
	private Socket s;
	private ObjectOutputStream oos;
	private ObjectInputStream ois;
	
	//Boolean
	private boolean populated = false;
	private AdminHomePage parent;

	public AdminAddToCourse(AdminHomePage parent, Socket s, ObjectOutputStream oos,
			ObjectInputStream ois) {
		super("SmartBoard");
		this.s = s;
		this.oos = oos;
		this.ois = ois;
		initVariables();
		createGUI();
		
		this.parent = parent;
	}

	public void initVariables() {
		// JPanels
		topPanel = new JPanel();
		middlePanel = new JPanel();
		bottomPanel = new JPanel();

		// JButtons
		createButton = new JButton("Add");
		createButton.addActionListener(this);
		backButton = new JButton("Back");
		backButton.addActionListener(this);
		// JLabels
		courseTitleLabel = new JLabel("Choose Course: ");
		addLabel = new JLabel("Choose User Type: ");
		pageTitleLabel = new JLabel("Add User to Course");
		chooseUserLabel = new JLabel("Choose User: ");

		// JComboboxes
		courseComboBox = new JComboBox();
		courseComboBox.addItem("Select...");
	//	courseComboBox.setSelectedIndex(0);

		userComboBox = new JComboBox();
		userComboBox.addItem("Select...");
	//	courseComboBox.setSelectedIndex(0);

		// JRadioButtons
		studentRad = new JRadioButton("Student");
		instructorRad = new JRadioButton("Instructor");
		buttonGroup = new ButtonGroup();
		buttonGroup.add(studentRad);
		buttonGroup.add(instructorRad);
		studentRad.addActionListener(this);
		instructorRad.addActionListener(this);

		// Colors
		darkBlue = new Color(70, 102, 117);
		lightBlue = new Color(148, 255, 252);
		pink = new Color(250, 200, 191);
	}

	public void createGUI() {
		this.setSize(1000, 700);
		this.setLocation(150, 60);
		this.setResizable(false);
		setCloseOp();

		// Set Fonts
		pageTitleLabel.setFont(new Font(fontName, Font.BOLD, fontSize));
		pageTitleLabel.setForeground(lightBlue);
		courseTitleLabel.setFont(new Font(fontName, Font.BOLD, fontSize2));
		courseTitleLabel.setForeground(lightBlue);
		addLabel.setFont(new Font(fontName, Font.BOLD, fontSize2));
		addLabel.setForeground(lightBlue);
		chooseUserLabel.setFont(new Font(fontName, Font.BOLD, fontSize2));
		chooseUserLabel.setForeground(lightBlue);

		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBackground(darkBlue);
		JPanel infoPanel = new JPanel();
		infoPanel.setBackground(darkBlue);

		JPanel titlePanel = new JPanel();
		titlePanel.add(pageTitleLabel);
		titlePanel.setBackground(darkBlue);

		JPanel buttonPanel = new JPanel();
		buttonPanel.add(backButton);
		buttonPanel.setBackground(darkBlue);

		topPanel.setLayout(new FlowLayout());
		topPanel.add(courseTitleLabel);
		topPanel.add(courseComboBox);
		topPanel.setBackground(darkBlue);
		middlePanel.setLayout(new FlowLayout(1, 20, 20));
		middlePanel.add(addLabel);
		middlePanel.add(studentRad);
		middlePanel.add(instructorRad);
		middlePanel.setBackground(darkBlue);
		bottomPanel.setLayout(new FlowLayout(1, 20, 20));
		bottomPanel.add(chooseUserLabel);
		bottomPanel.add(userComboBox);
		bottomPanel.setBackground(darkBlue);

		infoPanel.setLayout(new GridLayout(4, 1));
		// infoPanel.setBackground(pink);
		infoPanel.setSize(200, 200);
		infoPanel.add(titlePanel);
		infoPanel.add(topPanel);
		infoPanel.add(middlePanel);
		infoPanel.add(bottomPanel);

		mainPanel.setLayout(new BorderLayout(50, 50));
		mainPanel.add(buttonPanel, BorderLayout.NORTH);
		mainPanel.add(infoPanel, BorderLayout.CENTER);
		createButton.setBackground(pink);
		createButton.setOpaque(true);
		createButton.setMinimumSize(new Dimension(50, 50));
		createButton.setMaximumSize(new Dimension(50, 50));
		createButton.setPreferredSize(new Dimension(50, 50));
		mainPanel.add(createButton, BorderLayout.SOUTH);

		// Colors
		// mainPanel.setBackground(darkBlue);

		this.add(mainPanel);
	}

	private void setCloseOp() {
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				try {
					Request r = new Request("close", "close");
					oos.writeObject(r); // tell server to close client's
					ois.close();
					oos.close();
					System.out.println("Closed streams");
				} catch (IOException e1) {
					System.out
							.println("IOE in LoginPage addListeners() window operation: "
									+ e1.getMessage());
				} finally {
					System.exit(0);
				}
			}
		});
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == backButton) {
			buttonGroup.clearSelection();
			userComboBox.setSelectedIndex(0);
			this.setVisible(false);
			this.getDefaultCloseOperation();
			parent.setVisible(true);
		}
		if (e.getSource() == studentRad){
			Request r = new Request("get vector of student names", null);
			try {
				oos.writeObject(r);
				oos.flush();
			} catch (IOException e1) {
				System.out.println("IOE in AdminAddToCourse.studentRad click: " + e1.getMessage());
			}
		}
		if (e.getSource() == instructorRad){
			Request r = new Request("get vector of instructor names", null);
			try{
				oos.writeObject(r);
				oos.flush();
			} catch (IOException e1) {
				System.out.println("IOE in AdminAddToCourse.instructorRad click: " + e1.getMessage());
			}
		}
		if(e.getSource() == createButton){
			if(!courseComboBox.getSelectedItem().equals("Select...") && (instructorRad.isSelected() || studentRad.isSelected()) && !userComboBox.getSelectedItem().equals("Select...")){
				if(studentRad.isSelected()){
					//Get Student from Server
					Request r = new Request("get student for aatc", (String) userComboBox.getSelectedItem());
					try {
						oos.writeObject(r);
						oos.flush();
					} catch (IOException e1) {
						System.out.println("IOE in AdminAddToCourse.createButton clicked: " + e1.getMessage());
					}
				}
			}
		}
	}
	

	public void populateCourseComboBox(Vector<String> toSend) {
			courseComboBox.removeAllItems();
			courseComboBox.addItem("Select...");
			courseComboBox.setSelectedIndex(0);
			for (String courseName : toSend) {
				courseComboBox.addItem(courseName);
			}
	}

	public void populateUserComboBox(Vector<String> toSend) {
		userComboBox.removeAllItems();
		userComboBox.addItem("Select...");
		userComboBox.setSelectedIndex(0);
		for (String name : toSend){
			userComboBox.addItem(name);
		}
	}

	public void sendStudent(Student s) {
		Hashtable<String, Course> studentCourses = s.get_all_courses();
		String courseName = (String) courseComboBox.getSelectedItem();
		System.out.println("SEND STUDENT");
		Course c = studentCourses.get(courseName);
		if(c == null){
			System.out.println("WE GOOD WITH THIS STUDENT AND COURSE");
		}else{
			JOptionPane.showMessageDialog(this,
					"Error",
					"Student already enrolled in this course!", JOptionPane.ERROR_MESSAGE);
		}
	}

}
